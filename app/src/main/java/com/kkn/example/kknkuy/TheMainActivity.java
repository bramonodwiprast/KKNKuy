package com.kkn.example.kknkuy;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class TheMainActivity extends TabActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.themainactivity);
        TabHost window;
        window = (TabHost) findViewById(android.R.id.tabhost);

        TabSpec tab1 = window.newTabSpec("Tab 1");
        TabSpec tab2 = window.newTabSpec("Tag 2");
        TabSpec tab3 = window.newTabSpec("Tag 3");

        tab1.setIndicator("Home");
        tab1.setContent(new Intent(this, HomeActivity.class));
        tab2.setIndicator("Pengumuman");
        tab2.setContent(new Intent(this, PengumumanActivity.class));
        tab3.setIndicator("Oprec");
        tab3.setContent(new Intent(this, OprecActivity.class));

        window.addTab(tab1);
        window.addTab(tab2);
        window.addTab(tab3);

    }
}
